;;;

(in-package "NET.URI")

(eval-when (:load-toplevel :execute)
  (let ((char1 (digit-char-p #\2 16))
        (char2 (digit-char-p #\5 16)))
    (setf (sbit *reserved-path-characters*      (+ (* 16 char1) char2)) 1
          ;; (sbit *reserved-nss-characters*       (+ (* 16 char1) char2)) 1
          ;; (sbit *reserved-fragment-characters*  (+ (* 16 char1) char2)) 1
          ;; (sbit *reserved-authority-characters* (+ (* 16 char1) char2)) 1
          (sbit *reserved-path-conv-characters* (+ (* 16 char1) char2)) 1)))

#+test
(let ((char1 (digit-char-p #\2 16))
      (char2 (digit-char-p #\5 16)))
  (list (sbit *reserved-path-characters*      (+ (* 16 char1) char2))
        (sbit *reserved-nss-characters*       (+ (* 16 char1) char2))
        (sbit *reserved-fragment-characters*  (+ (* 16 char1) char2))
        (sbit *reserved-authority-characters* (+ (* 16 char1) char2))
        (sbit *reserved-path-conv-characters* (+ (* 16 char1) char2))))

#||
;;; Before Patch
NET.URI(3): #u"%25"
#<URI %>
;; After Patch
NET.URI(4): 
#<URI %25>
||#