;;; -*- coding:cp932 -*-

(eval-when (:load-toplevel :compile-toplevel :execute)
  (require :aserve))

(defpackage "NET.ASERVE"
  (:export "MULTIPART-HEADER-P" "GET-HOST-PORT"))

(in-package "NET.ASERVE")

;;; get-multipart-headerで日本語ファイル名が通らない問題
;;;
;;; (1) 横田からの調査報告
;;; 
;;; まず、今回の場合は <form> から生成されて送られる multipart/form-data
;;; のヘッダからファイル名を取り出す時点で、文字化けしてしまっていることが問題になっています。
;;; 
;;; その multipart/form-data から、ファイル名を取り出す処理は、aserve の get-multipart-header
;;; 関数 (https://github.com/franzinc/aserve/blob/master/main.cl#L2285)
;;; で行われています。
;;; 
;;; この中で、問題の文字化けを起こしている箇所は、 buffer-subseq-to-string 関数の呼び出し
;;; (https://github.com/franzinc/aserve/blob/master/main.cl#L2354) です。
;;; 
;;; この buffer-subseq-to-string 関数
;;; (https://github.com/franzinc/aserve/blob/master/headers.cl#L629) は、
;;; (unsigned-byte 8) のバイト部分列から文字列を作り出すのですが、なんと external-format を考慮せずに
;;; code-char で文字列を構築しています。結果として、 Latin-1 相当と見なして復号してしまっているので、 UTF-8
;;; を渡すと文字化けしてしまいます。
;;; 
;;; この multipart/form-data のヘッダの文字化け問題について、 aserve の問題かと思ったのですが、どうやら他の Web
;;; サーバでもよくある話のようです。 Google で「multipart form-data 日本語」で検索すると出てきます。
;;;
;;;
;;; (2) 黒田によるパッチ
;;; buffer-subseq-to-string は main.cl と header.cl とからしか呼ばれてゐない模樣
;;; 返り値に latin1 の string しか想定してゐないのであれば、ここに external-format 概念を持ち込んでも
;;; 上位互換的には問題がないだらうとの判斷により、buffer-subseq-to-string を以下の樣に再定義し直した
;;;
(defun buffer-subseq-to-string (buff start end)
  ;; extract a subsequence of the usb8 buff and return it as a string
  (octets-to-string buff :start start :end end :external-format *default-aserve-external-format*)
  #+ignore
  (let ((str (make-string (- end start))))
    (do ((i start (1+ i))
	 (ii 0 (1+ ii)))
	((>= i end))
      (setf (schar str ii) 
	(code-char (aref buff i))))
    str))

;;; :TIME
(net.html.generator::def-std-html :time t nil)

;;; check multipart post or not without side effect
(defmethod multipart-header-p ((req http-request))
  (let* ((ctype (header-slot-value req :content-type))
	 (parsed (and ctype (parse-header-value ctype))))
    (and (listp (car parsed))
         (listp (cdar parsed))
         (equalp "multipart/form-data" (cadar parsed)))))

;;;
(eval-when (:load-toplevel :execute)
  (setq *log-time-zone* excl::*time-zone*))

(defmethod logmess-stream (category level message stream
                           &optional (format *debug-format*))
  ;; send the log message to the given stream which should be a
  ;; stream object and not a stream indicator (like t)
  ;; If the stream has a lock use that.
  (declare (ignore level))
  (multiple-value-bind (csec cmin chour cday cmonth cyear)
      (decode-universal-time (get-universal-time) *log-time-zone*)
    (let* ((*print-pretty* nil)
	   (str (ecase format
                  (:long
                   (format
                    nil "[~A] ~A: ~4,'0D-~2,'0D-~2,'0DT~2,'0D:~2,'0D:~2,'0D - ~A~%"
                    category (mp:process-name sys:*current-process*)
                    cyear cmonth cday chour cmin csec
                    message))
                  (:brief
                   (format nil "~2,'0d:~2,'0d:~2,'0d - ~a~%" chour cmin csec
                           message))))
	   (lock (getf (excl::stream-property-list stream) :lock)))
      (if lock
	  (mp:with-process-lock (lock)
            (when (open-stream-p stream)
              (write-sequence str stream)
              (finish-output stream)))
        (progn
          (write-sequence str stream)
          (finish-output stream))))))